const express = require('express');
const routerPersonal = require('express').Router();
const model = require("../model/datahandler");

var errorInputMessage = {
                          "Eng" : "You have input invalid choices. Please try again",
                          "Hil" : "Sala nga choices ang imo gin-enter. Palihog liwat sang imo sabat."
}

routerPersonal.post('/personal_details', function(req, res) {
    console.log("routerPersonal post");
    console.log(req.body);
    var gender = req.body.sex;
    var children_ask = req.body.children_ask;
    var disease = req.body.disease;
    var medication = req.body.medication;
    var events = req.body.events;
    var lang = req.body.lang.substring(0,3);

    var jsonResponse = {};

    if(typeof disease !== "undefined") {
        if(disease.split(",").some(isNaN) &&
           disease.toLowerCase() != "none" &&
           disease.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Personal Sheet 2-0"]
			};
        } else {
            jsonResponse = {
                                "redirect_to_blocks": [lang + " Personal Sheet 3"]
			};
        }
    }

    if(typeof medication !== "undefined") {
        if(medication.split(",").some(isNaN) && 
           medication.toLowerCase() != "none" && 
           medication.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Personal Sheet 3-0"]
			};
        } else {
            jsonResponse = {
                                "redirect_to_blocks": [lang + " Personal Sheet 4"]
			};
        }
    }

    if(typeof events !== "undefined") {
        if(events.split(",").some(isNaN) &&
           events.toLowerCase() != "none" && 
           events.toLowerCase() != "wala") {
            jsonResponse = {
 				"messages": [
   				{"text": errorInputMessage [lang]}
 			    ],
                                "redirect_to_blocks": [lang + " Personal Sheet 5-0"]
			};
        } else {
            jsonResponse = {
                                "redirect_to_blocks": [lang + " Personal Sheet 6"]
			};
        }
    }

    if(gender == "Female" || gender == "Babayi") { 
	jsonResponse = {
	  "messages": [
	    {
	      "text": (lang == "Hil"? "Gabusong ka bala subong?" : "Are you currently pregnant?"),
	      "quick_replies": [
	        {
	          "title": (lang == "Hil"? "Oo" : "Yes"),
	          "set_attributes": {
	            "is_pregnant" : "Yes",
                    "children_ask" : "Yes"
	          }
	        },
	        {
	          "title": (lang == "Hil"? "Wala" : "No"),
	          "set_attributes": {
	            "is_pregnant": "no",
                    "children_ask" : "Yes"
	          }
	        }
	      ],
	      "quick_reply_options": {
	        "process_text_by_ai": false,
	        "text_attribute_name": "user_message"
	      }
	    }
	  ]
	};
    }
    else if (req.body.children_ask != "null" && typeof req.body.children_ask !== "undefined") { 
	jsonResponse = {
	  "messages": [
	    {
	      "text":  (lang == "Hil"? "May ara ka bala nga bata subong nga naga-edad nubo sa lima ka tu-ig?" : "Do you have children under 5 years old?"),
	      "quick_replies": [
	        {
	          "title":  (lang == "Hil"? "Oo" : "Yes"),
	          "set_attributes": {
	            "has_toddler": "Yes"
	          }
	        },
	        {
	          "title":  (lang == "Hil"? "Wala" : "No"),
	          "set_attributes": {
	            "has_toddler": "No"
	          }
	        }
	      ],
	      "quick_reply_options": {
	        "process_text_by_ai": false,
	        "text_attribute_name": "user_message"
	      }
	    }
	  ]
	};
    }
    console.log(jsonResponse);
    res.send(jsonResponse);
});

routerPersonal.post('/save_personal_details', function(req, res) {
    console.log("routerPersonal post");
    console.log(req.body);
   
    var jsonResponse = {};
    
    var height = req.body.height;
    var weight = req.body.weight;
    var disease = req.body.disease;
    var medications = req.body.medications;
    var is_smoking = req.body.is_smoking;
    var monthly_income = req.body.monthly_income;
    var child_count = req.body.child_count;
    var events = req.body.events;
    var messenger_id = req.body.messenger_id;

    var personalDetails = {
                            messenger_id : messenger_id,
                            height : height ,
                            weight : weight ,
                            disease : disease ,
                            medications : medications ,
                            is_smoking : is_smoking ,
                            monthly_income : monthly_income ,
                            child_count : child_count ,
                            events : events 
                           };


    model.save_data("personal", personalDetails);
    var jsonResponse = {
 			"messages": [
   					{"text": "Your personal identity data is saved!"}	
 				]
			};
    console.log(jsonResponse);
    res.send(jsonResponse);
});

module.exports = routerPersonal;