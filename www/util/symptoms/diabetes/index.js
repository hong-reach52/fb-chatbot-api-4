module.exports = (iso, survey) => {
    let response;

    switch (iso) {
        case "en":
            response = require("./lang/en")(survey)
            break;
        case "in":
            response = require("./lang/in")(survey)
            break;
        default:
            response = require("./lang/en")(survey)
            break;
    }

    return response
}